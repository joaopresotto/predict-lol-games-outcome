import os
import numpy as np
import data_utils as d
from pathlib import Path
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import joblib

# Read features and results from file
features = np.load(os.path.join("data", "lol-data.npz"))["features"]
results = np.load(os.path.join("data", "lol-data.npz"))["results"]

exported_pipeline = GradientBoostingClassifier(
    learning_rate=0.01,
    max_depth=5,
    max_features=0.3,
    min_samples_leaf=18,
    min_samples_split=9,
    n_estimators=100,
    subsample=0.8,
)
# Fix random state in exported estimator
if hasattr(exported_pipeline, "random_state"):
    setattr(exported_pipeline, "random_state", 42)

exported_pipeline.fit(features, results)
joblib.dump(exported_pipeline, os.path.join("data","model.pkl"))

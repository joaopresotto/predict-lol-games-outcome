import sys
import os
import tkinter as tk
import tkinter.ttk
import data_utils as d
import joblib
import numpy as np


class AutocompleteCombobox(tkinter.ttk.Combobox):
    def set_completion_list(self, completion_list):
        """Use our completion list as our drop down selection menu, arrows move through menu."""
        self._completion_list = sorted(
            completion_list, key=str.lower
        )  # Work with a sorted list
        self._hits = []
        self._hit_index = 0
        self.position = 0
        self.bind("<KeyRelease>", self.handle_keyrelease)
        self["values"] = self._completion_list  # Setup our popup menu

    def autocomplete(self, delta=0):
        """autocomplete the Combobox, delta may be 0/1/-1 to cycle through possible hits"""
        if (
            delta
        ):  # need to delete selection otherwise we would fix the current position
            self.delete(self.position, tkinter.END)
        else:  # set position to end so selection starts where textentry ended
            self.position = len(self.get())
        # collect hits
        _hits = []
        for element in self._completion_list:
            if element.lower().startswith(
                self.get().lower()
            ):  # Match case insensitively
                _hits.append(element)
        # if we have a new hit list, keep this in mind
        if _hits != self._hits:
            self._hit_index = 0
            self._hits = _hits
        # only allow cycling if we are in a known hit list
        if _hits == self._hits and self._hits:
            self._hit_index = (self._hit_index + delta) % len(self._hits)
        # now finally perform the auto completion
        if self._hits:
            self.delete(0, tkinter.END)
            self.insert(0, self._hits[self._hit_index])
            self.select_range(self.position, tkinter.END)

    def handle_keyrelease(self, event):
        """event handler for the keyrelease event on this widget"""
        if event.keysym == "BackSpace":
            self.delete(self.index(tkinter.INSERT), tkinter.END)
            self.position = self.index(tkinter.END)
        if event.keysym == "Left":
            if self.position < self.index(tkinter.END):  # delete the selection
                self.delete(self.position, tkinter.END)
            else:
                self.position = self.position - 1  # delete one character
                self.delete(self.position, tkinter.END)
        if event.keysym == "Right":
            self.position = self.index(tkinter.END)  # go to end (no selection)
        if len(event.keysym) == 1:
            self.autocomplete()
        # No need for up/down, we'll jump to the popup
        # list at the position of the autocompletion


def predictOutcome():
    if not (
        blue_top.get()
        and blue_jungle.get()
        and blue_mid.get()
        and blue_adc.get()
        and blue_sup.get()
    ):
        predictMessage = tk.Label(root, text="\tblue_team is missing!")
    elif not (
        red_top.get()
        and red_jungle.get()
        and red_mid.get()
        and red_adc.get()
        and red_sup.get()
    ):
        predictMessage = tk.Label(root, text="\tred_team is missing!")
    else:
        # Blue Team
        blue_team = []
        blue_team.append(blue_top.get())
        blue_team.append(blue_jungle.get())
        blue_team.append(blue_mid.get())
        blue_team.append(blue_adc.get())
        blue_team.append(blue_sup.get())

        # Red Team
        red_team = []
        red_team.append(red_top.get())
        red_team.append(red_jungle.get())
        red_team.append(red_mid.get())
        red_team.append(red_adc.get())
        red_team.append(red_sup.get())

        # Load model from file
        model = joblib.load(os.path.join("data", "model.pkl"))

        blue_team = d.get_features(blue_team)
        red_team = d.get_features(red_team)

        compositionA = np.append(blue_team, red_team).reshape(1, -1)
        compositionB = np.append(red_team, blue_team).reshape(1, -1)

        blue_results = model.predict_proba(compositionA.reshape(1, -1))
        red_results = model.predict_proba(compositionB.reshape(1, -1))

        blue_win_prob = blue_results[0][1]
        red_win_prob = red_results[0][1]

        if blue_win_prob > red_win_prob:
            predict_text = (
                "\tBlue team is winning with "
                + str(round(blue_win_prob * 100, 2))
                + "% probability."
            )
            predictMessage = tk.Label(
                root, text=predict_text, font=("helvetica", 12, "bold")
            )
        else:
            predict_text = (
                "\tRed team is winning with "
                + str(round(red_win_prob * 100, 2))
                + "% probability."
            )
            predictMessage = tk.Label(
                root, text=predict_text, font=("helvetica", 12, "bold")
            )

    canvas1.create_window(170, 230, window=predictMessage)


if __name__ == "__main__":
    root = tk.Tk()

    width = 400
    height = 260

    canvas1 = tk.Canvas(root, width=width, height=height, relief="raised")
    canvas1.pack()

    # Blue Team
    blue_team_label = tk.Label(
        root, text="Blue Team", font=("helvetica", 15, "bold"))
    canvas1.create_window(55, 20, window=blue_team_label)

    blue_top = AutocompleteCombobox(root)
    blue_top.set_completion_list(d.get_names())
    canvas1.create_window(90, 50, window=blue_top)

    blue_jungle = AutocompleteCombobox(root)
    blue_jungle.set_completion_list(d.get_names())
    canvas1.create_window(90, 70, window=blue_jungle)

    blue_mid = AutocompleteCombobox(root)
    blue_mid.set_completion_list(d.get_names())
    canvas1.create_window(90, 90, window=blue_mid)

    blue_adc = AutocompleteCombobox(root)
    blue_adc.set_completion_list(d.get_names())
    canvas1.create_window(90, 110, window=blue_adc)

    blue_sup = AutocompleteCombobox(root)
    blue_sup.set_completion_list(d.get_names())
    canvas1.create_window(90, 130, window=blue_sup)

    # Red Team
    red_team_label = tk.Label(root, text="Red Team",
                              font=("helvetica", 15, "bold"))
    canvas1.create_window(width - 50, 20, window=red_team_label)

    red_top = AutocompleteCombobox(root)
    red_top.set_completion_list(d.get_names())
    canvas1.create_window(width - 90, 50, window=red_top)

    red_jungle = AutocompleteCombobox(root)
    red_jungle.set_completion_list(d.get_names())
    canvas1.create_window(width - 90, 70, window=red_jungle)

    red_mid = AutocompleteCombobox(root)
    red_mid.set_completion_list(d.get_names())
    canvas1.create_window(width - 90, 90, window=red_mid)

    red_adc = AutocompleteCombobox(root)
    red_adc.set_completion_list(d.get_names())
    canvas1.create_window(width - 90, 110, window=red_adc)

    red_sup = AutocompleteCombobox(root)
    red_sup.set_completion_list(d.get_names())
    canvas1.create_window(width - 90, 130, window=red_sup)

    button1 = tk.Button(
        text="Compute Probalities",
        command=predictOutcome,
        bg="brown",
        fg="white",
        font=("helvetica", 15, "bold"),
    )
    canvas1.create_window(200, 180, window=button1)

    # canvas1.create_rectangle(1000, 200, 200, 300, fill="white", outline="white")
    # canvas1.create_rectangle(0, 200, 200, 300, fill="white", outline="white")

    root.mainloop()

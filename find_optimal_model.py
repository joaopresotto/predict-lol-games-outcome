# https://github.com/EpistasisLab/tpot

from tpot import TPOTClassifier
from sklearn.model_selection import train_test_split
import numpy as np
import os

features = np.load(os.path.join("data", "lol-data.npz"))["features"]
results = np.load(os.path.join("data", "lol-data.npz"))["results"]

X_train, X_test, y_train, y_test = train_test_split(
    features, results, train_size=0.8, test_size=0.2, random_state=42
)

tpot = TPOTClassifier(
    generations=10, population_size=50, verbosity=3, n_jobs=-1
)

tpot.fit(X_train, y_train)
print(tpot.score(X_test, y_test))
tpot.export("tpot_pipeline.py")


import data_utils as d

import os
import numpy as np
import pandas as pd


def read_csv_file(file_name):
    data = pd.read_csv(os.path.join("data", str(file_name) + ".csv"))

    print(data.columns)

    data = data[~(data["champion"].isna())]

    return data


# *********
data = read_csv_file("2020_LoL_esports_match_data_from_OraclesElixir_20200929")

# Get GameIDs in order to create features matrix
gameIDs = data.gameid.unique()

features = np.zeros((len(gameIDs), 2 * len(d.get_names())))
results = np.zeros(len(gameIDs))

for i in range(len(gameIDs)):
    blue_team = []
    blue_team_result = -1
    red_team = []
    red_team_result = -1
    for index, row in data[data["gameid"] == (str(gameIDs[i]))].iterrows():
        if row["side"] == "Blue":
            blue_team.append(row["champion"])
            if len(blue_team) == 5:
                blue_team_result = row["result"]
        else:  # row['side'] == "Red":
            red_team.append(row["champion"])
            if len(red_team) == 5:
                red_team_result = row["result"]

    # Convert team composition to a one-hot vector
    blue_feat = d.get_features(blue_team)
    red_feat = d.get_features(red_team)

    features[i] = np.append(blue_feat, red_feat)
    results[i] = blue_team_result

np.savez_compressed(os.path.join("data","lol-data"), features=features, results=results)

